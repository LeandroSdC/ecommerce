public class Produto {
    
    // propriedades automaticas:
    public int ProdutoId { get; set; }
    public string Nome { get; set; } 
    public decimal Preco { get; set; }
}