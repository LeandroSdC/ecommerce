public class Pedido {

  public int PedidoId{get; set;}
  public DateTime Data {get; set;}
  public decimal Total {
    get {
        decimal total = 0;
        foreach (Item item in this.Items) {
            total += item.Total;
        }
        return total;
    }
}
  public Cliente Cliente {get; set;}
  public List<Item> Items { get; set; }

}