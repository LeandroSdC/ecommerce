using System.Linq;

public class ClienteMemoryRepository : IClienteRepository
{
    List<Cliente> clientes = new List<Cliente>();
    public void Create(Cliente cliente)
    {
        clientes.Add(cliente);
    }

    public void Delete(int id)
    {
        var cliente = (from p in clientes 
                        where p.ClienteId == id 
                        select p)
                        .FirstOrDefault();      
        clientes.Remove(cliente);
    }

    public List<Cliente> Read()
    {
        return clientes;
    }

    public Cliente Read(int id)
    {
        return clientes.FirstOrDefault( p=> p.ClienteId == id);
    }

    public List<Cliente> Search(string searchKeyword)
    {
        return clientes.Where(
            c => c.Name.Contains(searchKeyword)).ToList();
    }

    public void Update(Cliente cliente)
    {
        var c = Read(cliente.ClienteId);

        c.Name = cliente.Name;
        c.Email = cliente.Email;
        c.Password = cliente.Password;
    }
}