using Microsoft.Data.SqlClient;

public abstract class Database : IDisposable
{
    protected SqlConnection conn;

    // abrir a conexão
    public Database() {

        string connectionString =  @"Data Source=DESKTOP-LAB0507\\SQLEXPRESS; 
        Initial Catalog=BDEcommerce; Integrated Security=true; TrustServerCertificate=True";

        conn = new SqlConnection(connectionString);
        conn.Open();

        Console.WriteLine("Conexão Aberta");
    }
    // fechar a conexão
    public void Dispose() {
        conn.Close();
        Console.WriteLine("Conexão Aberta");
    }
}